terraform {
  backend "s3" {
    bucket         = "terraform-remote-state-ap-south-1"
    key            = "recipe-app.tfstate"
    region         = "ap-south-1"
    encrypt        = "true"
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region = "ap-south-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}